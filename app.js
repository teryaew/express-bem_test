var Express = require('express');
var ExpressBem = require('express-bem');
var http = require('http');


// create app and bem
var app = Express();
var bem = ExpressBem({
  projectRoot: './', // bem project root, used for bem make only
  path: './' // path to your bundles
});

// here to lookup bundles at your path you need small patch
app.bem = bem.bindTo(app);

bem.usePlugin('express-bem-tools-make', { verbosity : 'debug' });

bem.engine('fullstack', '.bem', ['.bemhtml.js', '.bemtree.js'], function (name, options, cb) {
  var view = this;
  bem.usePlugin('express-bem-bemtree'); // requires module express-bem-bemtree
  bem.usePlugin('express-bem-bemhtml'); // ... express-bem-bemhtml

  // // pass options.bemjson directly to bemhtml
  // if (options.bemjson) return view.thru('bemhtmlEngine');

  // // return bemjson if requested
  // if (options.raw === true) return view.thru('bemtreeEngine');

  // full stack
  view.thru('bemtreeEngine', name, options, function (err, bemjson) {
    if (err) return cb(err);

    options.bemjson = bemjson;
    view.thru('bemhtmlEngine', name, options, function (err, data) {
      if (err) return cb(err);
      cb(null, data);
    });
  });
});


app.get('/', function (req, res) {
  res.render('desktop.bundles/index', {
    data: {
        page: {
            title: "Старт проекта на BEM-TOOLS",
            description: "Page description",
            current: "index"
        }
    }
  });
});


var server = http.createServer(app);
server.listen('3000', function() {
  console.log('Экспресс сервер слушает порт 3000');
});
