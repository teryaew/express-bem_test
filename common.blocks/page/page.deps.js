({
    mustDeps: [
        'i-bem',
        'reset-css'
    ],
    shouldDeps: [
        {
            block : 'page',
            mods : { view : [
                'index'
            ]}
        }
    ]
})
